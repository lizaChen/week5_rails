module MoviesHelper
  # Checks if a number is odd:
  def oddness(count)
    count.odd? ?  "odd" :  "even"
  end
  
  def sort_list(title,last_asc=false)
    @movies.sort_by!{|a|a.title.downcase}
    if(last_asc)
      @movies.reverse
    end
    @sort_seq = !last_asc
      
  end
end
